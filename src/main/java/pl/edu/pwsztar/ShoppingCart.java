package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<Product> products = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        if (quantity < 1 || products.size()+quantity > ShoppingCartOperation.PRODUCTS_LIMIT || price <= 0) {
            return false;
        }

        Optional<Product> optionalProduct = getProductByName(productName);
        if (optionalProduct.isPresent()) {
            if (optionalProduct.get().getPrice() == price) {
                optionalProduct.get().increaseQuantity(quantity);
            } else {
                return false;
            }
        } else {
            products.add(new Product(productName, price, quantity));
        }
        return true;

    }

    public boolean deleteProducts(String productName, int quantity) {

        for (Product product: products) {
            if (Objects.equals(product.getName(), productName)) {
                if (product.getQuantity() >= quantity && quantity > 0) {
                    product.decreaseQuantity(quantity);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        Optional<Product> optionalProduct = getProductByName(productName);
        return optionalProduct.map(Product::getQuantity).orElse(0);
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for (Product product: products) {
            int price = product.getPrice();
            int quantity = product.getQuantity();
            sum += price * quantity;
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        Optional<Product> optionalProduct = getProductByName(productName);
        return optionalProduct.map(Product::getPrice).orElse(0);
    }

    public List<String> getProductsNames() {
        List<String> productNames = new ArrayList<>();
        for (Product product: products) {
            productNames.add(product.getName());
        }
        return productNames;
    }

    private Optional<Product> getProductByName(String productName) {
        for (Product product: products) {
            if (product.getName().equals(productName)) {
                return Optional.of(product);
            }
        }
        return Optional.empty();
    }
}
